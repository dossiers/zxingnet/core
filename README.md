# ZXing .Net Core - Core Library
_Porting of ZXing.Net to .Net Core._


This is an effort to port [ZXing.Net](http://zxingnet.codeplex.com/) to .Net Core. _(Note that we are not affiliated with ZXing.Net or the original Java ZXing project in any way.)_

The goal of this project is to create a portable barcode scanning library for .Net Core platform. As such, our effort will be limited to porting the portion of ZXing.Net that is relevant to the modern platform. More specifically, we will support the followin platforms:

* .Net Core (Console)
* UWP (WinRT)
* ASP.Net Core
* Unity (Windows Universal)


_Nuget package available: [ZXing.Net.Core](https://www.nuget.org/packages/ZXing.Net.Core/)_


